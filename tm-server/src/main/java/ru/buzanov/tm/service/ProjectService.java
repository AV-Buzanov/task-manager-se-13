package ru.buzanov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    public ProjectService(ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public Project load(@Nullable String userId, @Nullable Project project) throws Exception {
        if (project == null || project.getId() == null || project.getName() == null)
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        project.setUserId(userId);
        project.setCreateDate(new Date(System.currentTimeMillis()));
        @NotNull final IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (projectRepository.isNameExist(userId, project.getName()))
                throw new Exception("This name already exist");
            if (projectRepository.findOne(userId, project.getId()) == null)
                projectRepository.load(project);
            else
                projectRepository.merge(userId, project.getId(), project);
            sqlSession.commit();
            return projectRepository.findOne(userId, project.getId());
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void load(@Nullable String userId, List<Project> list) throws Exception {
        for (Project project : list)
            load(userId, project);
    }

    @Override
    public @NotNull Collection<Project> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return projectRepository.findAll(userId);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Project> findByName(@Nullable String userId, @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (name == null || name.isEmpty())
            return new ArrayList<>();
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return projectRepository.findByName(userId, "%" + name + "%");
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Project> findByDescription(@Nullable String userId, @Nullable String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (desc == null || desc.isEmpty())
            return new ArrayList<>();
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return projectRepository.findByDescription(userId, "%" + desc + "%");
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project findOne(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return projectRepository.findOne(userId, id);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean isNameExist(@Nullable String userId, @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return projectRepository.isNameExist(userId, name);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable String userId, @Nullable String id, @Nullable Project project) throws Exception {
        if (project == null || project.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (projectRepository.findOne(userId, id) != null)
                projectRepository.merge(userId, id, project);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Project> findAllOrdered(@Nullable String userId, boolean dir, @NotNull Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (field == null)
            return new ArrayList<>();
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return projectRepository.findAllOrdered(userId, field);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project remove(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            Project project = projectRepository.findOne(userId, id);
            projectRepository.remove(userId, id);
            sqlSession.commit();
            return project;
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IProjectRepository projectRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            projectRepository.removeAll(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    private IProjectRepository openTransaction() {
        sqlSession = serviceLocator.getSqlSessionFactory().openSession();
        return sqlSession.getMapper(IProjectRepository.class);
    }
}