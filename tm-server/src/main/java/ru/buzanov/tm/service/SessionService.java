package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.entity.Session;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public Session load(@Nullable Session session) throws Exception {
        if (session == null || session.getId() == null || session.getUserId() == null || session.getSignature() == null)
            return null;
        @NotNull final ISessionRepository sessionRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (sessionRepository.findOne(session.getId()) == null)
                sessionRepository.load(session);
            else
                sessionRepository.merge(session.getId(), session);
            sqlSession.commit();
            return sessionRepository.findOne(session.getId());
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Session> findAll() throws Exception {
        ISessionRepository sessionRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return sessionRepository.findAll();
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void load(@Nullable List<Session> list) throws Exception {
        if (list == null)
            throw new Exception("Argument cant be empty or null.");
        for (Session session : list)
            load(session);
    }

    @Nullable
    @Override
    public Session findOne(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty())
            return null;
        ISessionRepository sessionRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return sessionRepository.findOne(id);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable String id, @Nullable Session session) throws Exception {
        if (session == null || session.getId() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ISessionRepository sessionRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (sessionRepository.findOne(id) != null)
                sessionRepository.merge(id, session);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session remove(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ISessionRepository sessionRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            Session session = sessionRepository.findOne(id);
            sessionRepository.remove(id);
            sqlSession.commit();
            return session;
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        ISessionRepository sessionRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            sessionRepository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    private ISessionRepository openTransaction() {
        sqlSession = serviceLocator.getSqlSessionFactory().openSession();
        return sqlSession.getMapper(ISessionRepository.class);
    }
}
