package ru.buzanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IService;
import ru.buzanov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {
    @NotNull
    protected ServiceLocator serviceLocator;
    @Nullable
    protected SqlSession sqlSession;

    public AbstractService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract T load(@Nullable final T entity) throws Exception;

    @NotNull
    public abstract Collection<T> findAll() throws Exception;

    public abstract void load(@Nullable final List<T> list) throws Exception;

    @Nullable
    public abstract T findOne(@Nullable final String id) throws Exception;

    public abstract void merge(@Nullable final String id, @Nullable final T entity) throws Exception;

    @Nullable
    public abstract T remove(@Nullable final String id) throws Exception;

    public abstract void removeAll() throws Exception;
}
