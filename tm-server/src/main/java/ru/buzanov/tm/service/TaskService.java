package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractWBSService<Task> implements ITaskService {

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public Task load(@Nullable String userId, @Nullable Task task) throws Exception {
        if (task == null || task.getId() == null || task.getName() == null)
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        task.setUserId(userId);
        task.setCreateDate(new Date(System.currentTimeMillis()));
        @NotNull final ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (taskRepository.isNameExist(userId, task.getName()))
                throw new Exception("This name already exist");
            if (taskRepository.findOne(userId, task.getId()) == null)
                taskRepository.load(task);
            else
                taskRepository.merge(userId, task.getId(), task);
            sqlSession.commit();
            return taskRepository.findOne(userId, task.getId());
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void load(@Nullable String userId, List<Task> list) throws Exception {
        for (Task task : list)
            load(userId, task);
    }

    @Override
    public @NotNull Collection<Task> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.findAll(userId);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Task> findByName(@Nullable String userId, @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (name == null || name.isEmpty())
            return new ArrayList<>();
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.findByName(userId, "%" + name + "%");
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Task> findByDescription(@Nullable String userId, @Nullable String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (desc == null || desc.isEmpty())
            return new ArrayList<>();
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.findByDescription(userId, "%" + desc + "%");
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task findOne(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.findOne(userId, id);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean isNameExist(@Nullable String userId, @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.isNameExist(userId, name);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable String userId, @Nullable String id, @Nullable Task task) throws Exception {
        if (task == null || task.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (taskRepository.findOne(userId, id) != null)
                taskRepository.merge(userId, id, task);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<Task> findAllOrdered(@Nullable String userId, boolean dir, @NotNull Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (field == null)
            return new ArrayList<>();
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.findAllOrdered(userId, field);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task remove(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            Task task = taskRepository.findOne(userId, id);
            taskRepository.remove(userId, id);
            sqlSession.commit();
            return task;
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            taskRepository.removeAll(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public Collection<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (projectId == null || projectId.isEmpty())
            return new ArrayList<>();
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return taskRepository.findByProjectId(userId, projectId);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (projectId == null || projectId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        ITaskRepository taskRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            taskRepository.removeByProjectId(userId, projectId);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    private ITaskRepository openTransaction() {
        sqlSession = serviceLocator.getSqlSessionFactory().openSession();
        ITaskRepository projectRepository = sqlSession.getMapper(ITaskRepository.class);
        return projectRepository;
    }
}