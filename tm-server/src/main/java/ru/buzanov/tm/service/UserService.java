package ru.buzanov.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.util.PasswordUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @Nullable
    public User load(@Nullable final User user) throws Exception {
        if (user == null || user.getId() == null || user.getLogin() == null || user.getPasswordHash() == null)
            return null;
        @NotNull final IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (userRepository.isLoginExist(user.getLogin()))
                throw new Exception("This login already exist");
            user.setPasswordHash(PasswordUtil.hashingPass(user.getPasswordHash(), user.getId()));
            user.setRoleType(RoleType.USER);
            if (userRepository.findOne(user.getId()) == null)
                userRepository.load(user);
            else
                userRepository.merge(user.getId(), user);
            sqlSession.commit();
            return userRepository.findOne(user.getId());
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void load(@Nullable List<User> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        for (User user : list)
            load(user);
    }

    @Override
    public void merge(@Nullable final String id, @Nullable final User user) throws Exception {
        if (user == null || user.getLogin() == null || user.getPasswordHash() == null || user.getRoleType() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            if (!userRepository.findOne(id).getLogin().equals(user.getLogin()))
                if (userRepository.isLoginExist(user.getLogin()))
                    throw new Exception("This login already exist.");
                user.setId(id);
            if (!userRepository.findOne(id).getPasswordHash().equals(user.getPasswordHash()))
                user.setPasswordHash(PasswordUtil.hashingPass(user.getPasswordHash(), user.getId()));
            if (userRepository.findOne(id) != null)
                userRepository.merge(id, user);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User findOne(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty())
            return null;
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return userRepository.findOne(id);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty())
            return null;
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return userRepository.findByLogin(login);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public Collection<User> findByRole(@Nullable final RoleType role) throws Exception {
        if (role == null)
            return null;
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return userRepository.findByRole(role);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull Collection<User> findAll() throws Exception {
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return userRepository.findAll();
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User remove(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            User user = userRepository.findOne(id);
            userRepository.remove(id);
            sqlSession.commit();
            return user;
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            userRepository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    public boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) throws Exception {
        if (login == null || login.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (pass == null || pass.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            User user = userRepository.findByLogin(login);
            if (user==null)
                return false;
            return userRepository.isPassCorrect(login, PasswordUtil.hashingPass(pass, user.getId()));
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty())
            throw new Exception("Argument can't be empty or null");
        IUserRepository userRepository = openTransaction();
        if (sqlSession == null)
            throw new Exception("Database sqlSession error.");
        try {
            return userRepository.isLoginExist(login);
        } catch (SQLException e) {
            throw new Exception("SQL error");
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public String getList() throws Exception {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        Collection<User> list = findAll();
        for (User user : list) {
            s.append(indexBuf).append(". ").append(user.getLogin());
            if (list.size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        int indexBuf = 1;
        Collection<User> list = findAll();
        for (User entity : list) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    @NotNull
    private IUserRepository openTransaction() {
        sqlSession = serviceLocator.getSqlSessionFactory().openSession();
        return sqlSession.getMapper(IUserRepository.class);
    }
}