package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.SessionService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.PropertyService;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final IProjectService projectService = new ProjectService(this);
    @NotNull
    private final ITaskService taskService = new TaskService(this);
    @NotNull
    private final IUserService userService = new UserService(this);
    @NotNull
    private final ISessionService sessionService = new SessionService(this);
    @NotNull
    private final PropertyService propertyService = new PropertyService();
    @NotNull
    private final String adress = "http://0.0.0.0:8080/";

    public void start() {
        Thread sessionCleanThread = new SessionCleanThread(this);
        sessionCleanThread.setDaemon(true);
        sessionCleanThread.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Endpoint.publish(adress + "ProjectEndpoint?wsdl", new ProjectEndpoint(this));
        Endpoint.publish(adress + "TaskEndpoint?wsdl", new TaskEndpoint(this));
        Endpoint.publish(adress + "AdminUserEndpoint?wsdl", new AdminUserEndpoint(this));
        Endpoint.publish(adress + "SessionEndpoint?wsdl", new SessionEndpoint(this));
        Endpoint.publish(adress + "UserEndpoint?wsdl", new UserEndpoint(this));
        System.out.println("Task manager server is running.");
        System.out.println(adress + "ProjectEndpoint?wsdl");
        System.out.println(adress + "TaskEndpoint?wsdl");
        System.out.println(adress + "AdminUserEndpoint?wsdl");
        System.out.println(adress + "SessionEndpoint?wsdl");
        System.out.println(adress + "UserEndpoint?wsdl");

        while (true) {
            try {
                if ("exit".equals(reader.readLine())) {
                    System.exit(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = propertyService.getJdbcUsername();
        @Nullable final String password = propertyService.getJdbcPassword();
        @Nullable final String url = propertyService.getJdbcUrl();
        @Nullable final String driver = propertyService.getJdbcDriver();
        final DataSource dataSource =
                new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}