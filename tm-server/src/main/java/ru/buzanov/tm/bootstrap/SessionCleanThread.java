package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;

import java.sql.SQLException;
import java.util.Properties;

public class SessionCleanThread extends Thread {
    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionCleanThread(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(serviceLocator.getPropertyService().getSessionLifetime() * 2);
                for (Session session : serviceLocator.getSessionService().findAll()) {
                    long timeDif = System.currentTimeMillis() - session.getCreateDate().getTime();
                    if (timeDif > serviceLocator.getPropertyService().getSessionLifetime())
                        serviceLocator.getSessionService().remove(session.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
