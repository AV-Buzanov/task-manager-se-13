package ru.buzanov.tm.api.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.Collection;

public interface IProjectRepository {
    @NotNull
    @Select("SELECT*FROM project WHERE userId=#{userId}")
    Collection<Project> findAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Nullable
    @Select("SELECT*FROM project WHERE userId=#{userId} AND id=#{id}")
    Project findOne(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Select("SELECT COUNT(*)>0 FROM project WHERE userId=#{userId} AND name=#{name}")
    boolean isNameExist(final @NotNull @Param("userId") String userId, final @NotNull @Param("name") String name) throws SQLException;

    @Update("UPDATE project SET name=#{project.name}, startDate=#{project.startDate}, finishDate=#{project.finishDate}, " +
            "description=#{project.description}, status=#{project.status} WHERE id=#{id} AND userId=#{userId}")
    void merge(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id, final @NotNull @Param("project") Project project) throws SQLException;

    @Update("DELETE FROM project WHERE id=#{id} AND userId=#{userId}")
    void remove(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Update("DELETE FROM project WHERE userId=#{userId}")
    void removeAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Select("SELECT*FROM project WHERE userId=#{userId} AND description LIKE #{desc}")
    @NotNull Collection<Project> findByDescription(final @NotNull @Param("userId") String userId, final @NotNull @Param("desc") String desc) throws SQLException;

    @Select("SELECT*FROM project WHERE userId=#{userId} AND name LIKE #{name}")
    @NotNull Collection<Project> findByName(final @NotNull @Param("userId") String userId, final @NotNull @Param("name") String name) throws SQLException;

    @Select("SELECT*FROM project WHERE userId=#{userId} ORDER BY #{field}")
    @NotNull Collection<Project> findAllOrdered(final @NotNull @Param("userId") String userId, final @NotNull @Param("field") Field field) throws SQLException;

    @Insert("INSERT INTO project (id, name, description, createDate, startDate, finishDate, userId, status) " +
            "VALUES (#{id},#{name},#{description},#{createDate},#{startDate},#{finishDate},#{userId},#{status})")
    void load(final @NotNull Project entity) throws SQLException;
}

