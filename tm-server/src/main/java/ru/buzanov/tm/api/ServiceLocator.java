package ru.buzanov.tm.api;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.util.PropertyService;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull ISessionService getSessionService();

    @NotNull PropertyService getPropertyService();

    @NotNull SqlSessionFactory getSqlSessionFactory();
}
