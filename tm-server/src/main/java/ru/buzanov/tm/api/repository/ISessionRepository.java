package ru.buzanov.tm.api.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.Session;

import java.sql.SQLException;
import java.util.Collection;

public interface ISessionRepository {
    @Select("SELECT*FROM session")
    @NotNull Collection<Session> findAll() throws SQLException;

    @Select("SELECT*FROM session WHERE id=#{id}")
    @Nullable Session findOne(final @NotNull @Param("id") String id) throws SQLException;

    @Update("UPDATE session SET userId=#{session.userId}, signature=#{session.signature}, createDate=#{session.createDate} WHERE id=#{id}")
    void merge(final @NotNull @Param("id") String id, final @NotNull @Param("session") Session session) throws SQLException;

    @Update("DELETE FROM session WHERE id=#{id}")
    void remove(final @NotNull @Param("id") String id) throws SQLException;

    @Update("DELETE FROM session")
    void removeAll() throws SQLException;

    @Insert("INSERT INTO session (id, userId, signature, createDate) " +
            "VALUES (#{id},#{userId},#{signature},#{createDate})")
    void load(@NotNull final Session session) throws SQLException;
}
