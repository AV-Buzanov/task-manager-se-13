package ru.buzanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository {
    @Select("SELECT*FROM user")
    @NotNull Collection<User> findAll() throws SQLException;

    @Results(id = "userResult", value = {
            @Result(property = "passwordHash", column = "password")
    })
    @Select("SELECT*FROM user WHERE id=#{id}")
    @Nullable User findOne(final @NotNull @Param("id") String id) throws SQLException;

    @Update("UPDATE user SET login=#{user.login}, password=#{user.passwordHash}, name=#{user.name}, " +
            "roleType=#{user.roleType} WHERE id=#{id}")
    void merge(final @NotNull @Param("id") String id, final @NotNull @Param("user") User user) throws SQLException;

    @Update("DELETE FROM user WHERE id=#{id}")
    void remove(final @NotNull @Param("id") String id) throws SQLException;

    @Update("DELETE FROM user")
    void removeAll() throws SQLException;

    @Insert("INSERT INTO user (id, name, login, password, roleType) " +
            "VALUES (#{id},#{name},#{login},#{passwordHash},#{roleType})")
    void load(@NotNull final User entity) throws SQLException;

    @Select("SELECT*FROM user WHERE login=#{login}")
    @Nullable User findByLogin(@NotNull @Param("login") final String login) throws SQLException;

    @Select("SELECT COUNT(*)>0 FROM user WHERE login=#{login} AND password=#{pass}")
    boolean isPassCorrect(@NotNull @Param("login") final String login, @NotNull @Param("pass") final String pass) throws SQLException;

    @Select("SELECT COUNT(*)>0 FROM user WHERE login=#{login}")
    boolean isLoginExist(@NotNull @Param("login") final String login) throws SQLException;

    @Select("SELECT*FROM user WHERE roleType=#{roleType}")
    @NotNull Collection<User> findByRole(@NotNull @Param("roleType") final RoleType role) throws SQLException;
}
