package ru.buzanov.tm.api.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.Collection;

public interface ITaskRepository {
    @NotNull
    @Select("SELECT*FROM task WHERE userId=#{userId}")
    Collection<Task> findAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Nullable
    @Select("SELECT*FROM task WHERE userId=#{userId} AND id=#{id}")
    Task findOne(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Select("SELECT COUNT(*)>0 FROM task WHERE userId=#{userId} AND name=#{name}")
    boolean isNameExist(final @NotNull @Param("userId") String userId, final @NotNull @Param("name") String name) throws SQLException;

    @Update("UPDATE task SET name=#{task.name}, startDate=#{task.startDate}, finishDate=#{task.finishDate}, " +
            "description=#{task.description}, status=#{task.status}, projectId=#{task.projectId} WHERE id=#{id} AND userId=#{userId}")
    void merge(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id, final @NotNull @Param("task") Task task) throws SQLException;

    @Update("DELETE FROM task WHERE id=#{id} AND userId=#{userId}")
    void remove(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Update("DELETE FROM task WHERE userId=#{userId}")
    void removeAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Select("SELECT*FROM task WHERE userId=#{userId} AND description LIKE #{desc}")
    @NotNull Collection<Task> findByDescription(final @NotNull @Param("userId") String userId, final @NotNull @Param("desc") String desc) throws SQLException;

    @Select("SELECT*FROM task WHERE userId=#{userId} AND name LIKE #{name}")
    @NotNull Collection<Task> findByName(final @NotNull @Param("userId") String userId, final @NotNull @Param("name") String name) throws SQLException;

    @Select("SELECT*FROM task WHERE userId=#{userId} ORDER BY #{field}")
    @NotNull Collection<Task> findAllOrdered(@NotNull @Param("userId") String userId, @NotNull @Param("field") Field field) throws SQLException;

    @Insert("INSERT INTO task (id, name, description, createDate, startDate, finishDate, userId, status, projectId) " +
            "VALUES (#{id},#{name},#{description},#{createDate},#{startDate},#{finishDate},#{userId},#{status}, #{projectId})")
    void load(final @NotNull Task entity) throws SQLException;

    @Select("SELECT*FROM task WHERE userId=#{userId} AND projectId=#{projectId}")
    @NotNull Collection<Task> findByProjectId(@NotNull @Param("userId") final String userId, @NotNull @Param("projectId") final String projectId) throws SQLException;

    @Update("DELETE FROM task WHERE projectId=#{projectId} AND userId=#{userId}")
    void removeByProjectId(@NotNull @Param("userId") final String userId, @NotNull @Param("projectId") final String projectId) throws SQLException;
}
