package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {
    @Nullable
    private String name;
    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    private RoleType roleType;
}
