package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractEntity {
    @Nullable
    private String userId;
    @NotNull
    private Date createDate = new Date(System.currentTimeMillis());
    @Nullable
    private String signature;
}
