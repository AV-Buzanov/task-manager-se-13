package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;

public class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO REMOVE]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        terminalService.printLine(projectService.getListP(session));
        @Nullable String idBuf = projectService.getIdByCountP(session, Integer.parseInt(terminalService.readLine()));
        projectService.removeP(session, idBuf);
//        taskService.removeByProjectIdT(session, idBuf);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
