package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Project;

public class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
//        for (@NotNull final Project project : projectService.findAllP(serviceLocator.getCurrentSession()))
//            taskService.removeByProjectIdT(serviceLocator.getCurrentSession(), project.getId());
        projectService.removeAllP(serviceLocator.getCurrentSession());
        terminalService.printLineG("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
