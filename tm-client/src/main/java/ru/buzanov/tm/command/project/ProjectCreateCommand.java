package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Project;

public class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Project project = new Project();
        terminalService.printLineG("[PROJECT CREATE]");
        terminalService.printG("[ENTER NAME] ");
        @NotNull final String name = terminalService.readLine();
        if (projectService.isNameExistP(serviceLocator.getCurrentSession(), name)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        project.setName(name);
        terminalService.printLineG("[FILL DATA(Y/N)?]");
        if (terminalService.readLine().equals("Y"))
            terminalService.readWBS(project);
        projectService.loadP(serviceLocator.getCurrentSession(), project);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
