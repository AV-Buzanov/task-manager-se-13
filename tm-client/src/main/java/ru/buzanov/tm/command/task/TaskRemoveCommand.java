package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;

import java.util.Objects;

public class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE TASK TO REMOVE]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        terminalService.printLine(taskService.getListT(session));
        @Nullable String idBuf = taskService.getIdByCountT(session, Integer.parseInt(terminalService.readLine()));
        taskService.removeT(session, Objects.requireNonNull(idBuf));
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
