package ru.buzanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class DataBinSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.load(serviceLocator);
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "Data.bin");
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(dto);
            terminalService.printLineG("[DATA SAVED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
