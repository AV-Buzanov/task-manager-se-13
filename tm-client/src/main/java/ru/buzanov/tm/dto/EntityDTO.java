package ru.buzanov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.endpoint.Exception_Exception;
import ru.buzanov.tm.endpoint.Project;
import ru.buzanov.tm.endpoint.Task;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Project> projects;
    private List<Task> tasks;

    public void load(@Nullable final ServiceLocator serviceLocator) throws Exception_Exception {
        if (serviceLocator == null) return;
        this.projects = new ArrayList<>(serviceLocator.getProjectEndpoint().findAllP(serviceLocator.getCurrentSession()));
        this.tasks = new ArrayList<>(serviceLocator.getTaskEndpoint().findAllT(serviceLocator.getCurrentSession()));
    }

    public void unLoad(@Nullable final ServiceLocator serviceLocator) throws Exception_Exception {
        if (serviceLocator == null) return;
        serviceLocator.getProjectEndpoint().loadListP(serviceLocator.getCurrentSession(), projects);
        serviceLocator.getTaskEndpoint().loadListT(serviceLocator.getCurrentSession(), tasks);
    }
}
